package ulco.cardGame.common.games;

import ulco.cardGame.common.games.boards.CardBoard;
import ulco.cardGame.common.games.components.Card;
import ulco.cardGame.common.games.components.Component;
import ulco.cardGame.common.interfaces.Player;

import java.io.*;
import java.net.Socket;
import java.util.*;

/**
 * CardGame which extend BoardGame
 * BoardGame need to implement Game interface methods
 */
public class CardGame extends BoardGame {

    private List<Card> cards;
    private Integer numberOfRounds;

    /**
     * Enable constructor of CardGame
     * - Name of the game
     * - Maximum number of players of the Game
     *  - Filename of current Game
     * @param name
     * @param maxPlayers
     * @param filename
     */
    public CardGame(String name, Integer maxPlayers, String filename) {
        super(name, maxPlayers, filename);

        this.board = new CardBoard();
        this.numberOfRounds = 0;
    }

    @Override
    public void initialize(String filename) {

        this.cards = new ArrayList<>();

        // Here initialize the list of Cards
        try {
            File cardFile = new File(filename);
            Scanner myReader = new Scanner(cardFile);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                String[] dataValues = data.split(";");

                // get Card value
                Integer value = Integer.valueOf(dataValues[1]);
                this.cards.add(new Card(dataValues[0], value, true));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    @Override
    public Player run() {

        Player gameWinner = null;

        // prepare to distribute card to each player
        Collections.shuffle(cards);

        int playerIndex = 0;

        for (Card card : cards) {

            players.get(playerIndex).addComponent(card);
            card.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }


        // Send update of Game state for each player
        for (Player player : players) {
            System.out.println(player.getName() + " has " + player.getComponents().size() + " cards");
        }

        // while each player can play
        while (!this.end()) {

            Map<Player, Card> playedCard = new HashMap<>();

            for (Player player : players) {

                if (!player.isPlaying())
                    continue;

                // Get card played by current player
                Card card = (Card) player.play();

                // add card inside board
                board.addComponent(card);

                // Keep knowledge of card played
                playedCard.put(player, card);

                System.out.println(player.getName() + " has played " + card.getName());
            }

            // display board state
            board.displayState();

            // Check which player has win
            int bestValueCard = 0;

            List<Player> possibleWinner = new ArrayList<>();
            List<Card> possibleWinnerCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // check if equality
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    possibleWinner.add(entry.getKey());
                    possibleWinnerCards.add(currentCard);
                }
            }

            // default winner index
            int winnerIndex = 0;

            // Random choice if equality is reached
            if (possibleWinner.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(possibleWinner.size() - 1);
            }

            Player roundWinner = possibleWinner.get(winnerIndex);
            Card winnerCard = possibleWinnerCards.get(winnerIndex);

            System.out.println("Player " + roundWinner + " won the round with " + winnerCard);

            // Update Game state
            for (Card card : playedCard.values()) {

                // remove from previous player
                roundWinner.addComponent(card);
                card.setPlayer(roundWinner);
            }

            // clear board state
            board.clear();

            // Check players State
            for (Player player : players){

                // player cannot still play if no card in hand
                if (player.getScore() == 0)
                    player.canPlay(false);

                // player win if all cards are in his hand
                if (player.getScore() == cards.size()) {
                    player.canPlay(false);
                    gameWinner = player;
                }
            }

            // Display Game state
            this.displayState();

            // shuffle player hand every n rounds
            this.numberOfRounds += 1;

            if (this.numberOfRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }
            }
        }

        return gameWinner;
    }

    public Player run(Map<Player, Socket> playerSockets) throws IOException, ClassNotFoundException {

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
            playerOos.writeObject("Game start");
        }

        Player gameWinner = null;

        // prepare to distribute card to each player
        Collections.shuffle(cards);

        int playerIndex = 0;

        for (Card card : cards) {

            players.get(playerIndex).addComponent(card);
            card.setPlayer(players.get(playerIndex));

            playerIndex++;

            if (playerIndex >= players.size()) {
                playerIndex = 0;
            }
        }

        for (Socket playerSocket : playerSockets.values()) {
            ObjectOutputStream playerGame = new ObjectOutputStream(playerSocket.getOutputStream());
            playerGame.writeObject(this);
        }


        // Send update of Game state for each player

        for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
            ObjectOutputStream playercards = new ObjectOutputStream(entry.getValue().getOutputStream());
            playercards.writeObject("You have " + entry.getKey().getComponents().size() + "cards");
        }

//        for (Player player : players) {
//            System.out.println(player.getName() + " has " + player.getComponents().size() + " cards");
//        }

        // while each player can play
        while (!this.end()) {

            Map<Player, Card> playedCard = new HashMap<>();

            for (Player player : players) {

                if (!player.isPlaying())
                    continue;

                // Get card played by current player
                String msg = "[" + player.getName() + "] is your turn to play";
                ObjectOutputStream sendmsg = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                sendmsg.writeObject(msg);

                System.out.println("Waiting for " + player.getName() + " to play...");

                for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                    if(entry.getKey()!=player){
                        ObjectOutputStream Oos = new ObjectOutputStream(entry.getValue().getOutputStream());
                        Oos.writeObject("Waiting for " + player.getName() + " to play...");
                    }
                }

                player.play(playerSockets.get(player));

                Object answer;
                Card card = null;

                do {
                    // Read and display the response message sent by server application
                    ObjectInputStream ois = new ObjectInputStream(playerSockets.get(player).getInputStream());
                    answer = ois.readObject();
                    if (answer instanceof Card)
                        card = (Card)answer;
                } while (!(answer instanceof Card));
                ObjectOutputStream oos3 = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                oos3.writeObject("You played " + card.getName());
//                pour chaque carte du jeu:
//                si valeur_carte_client == valeur_carte_jeu
//                supprimer la carte du jeu

                for(Component card1 : player.getSpecificComponents(Card.class)){
                    if(card1.getId().equals(card.getId())){
                        player.removeComponent(card1);
                    }
                }

                //Card card = (Card) player.play();

                // add card inside board
                board.addComponent(card);

                // Keep knowledge of card played
                playedCard.put(player, card);

                System.out.println(player.getName() + " has played " + card.getName());
            }

            // display board state
            //board.displayState();
            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream sendboard = new ObjectOutputStream(entry.getValue().getOutputStream());
                sendboard.writeObject(board);
            }

            // Check which player has win
            int bestValueCard = 0;

            List<Player> possibleWinner = new ArrayList<>();
            List<Card> possibleWinnerCards = new ArrayList<>();

            for (Card card : playedCard.values()) {
                if (card.getValue() >= bestValueCard)
                    bestValueCard = card.getValue();
            }

            // check if equality
            for(Map.Entry<Player, Card> entry : playedCard.entrySet()){

                Card currentCard = entry.getValue();

                if (currentCard.getValue() >= bestValueCard) {

                    possibleWinner.add(entry.getKey());
                    possibleWinnerCards.add(currentCard);
                }
            }

            // default winner index
            int winnerIndex = 0;

            // Random choice if equality is reached
            if (possibleWinner.size() > 1){
                Random random = new Random();
                winnerIndex = random.nextInt(possibleWinner.size() - 1);
            }

            Player roundWinner = possibleWinner.get(winnerIndex);
            Card winnerCard = possibleWinnerCards.get(winnerIndex);

            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream sendWinner = new ObjectOutputStream(entry.getValue().getOutputStream());
                sendWinner.writeObject("Player " + roundWinner + " won the round with " + winnerCard);
            }

            System.out.println("Player " + roundWinner + " won the round with " + winnerCard);

            // Update Game state
            for (Card card : playedCard.values()) {

                // remove from previous player
                roundWinner.addComponent(card);
                card.setPlayer(roundWinner);
            }

            // clear board state
            board.clear();

            // Check players State
            for (Player player : players){

                // player cannot still play if no card in hand
                if (player.getScore() == 0){
                    player.canPlay(false);
                    ObjectOutputStream sendLose = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                    sendLose.writeObject("YOU LOSE !!! (you can't play so :-( )");
                }


                // player win if all cards are in his hand
                if (player.getScore() == cards.size()) {
                    player.canPlay(false);
                    ObjectOutputStream sendWin = new ObjectOutputStream(playerSockets.get(player).getOutputStream());
                    sendWin.writeObject("YOU WIN !!!");
                    gameWinner = player;
                }
            }

            // shuffle player hand every n rounds
            this.numberOfRounds += 1;

            if (this.numberOfRounds % 10 == 0) {
                for (Player player : players){
                    player.shuffleHand();
                }
            }

            // Display Game state
            //this.displayState();
            for(Map.Entry<Player,Socket>entry : playerSockets.entrySet()){
                ObjectOutputStream updateGame = new ObjectOutputStream(entry.getValue().getOutputStream());
                updateGame.writeObject(this);
            }


        }

        return gameWinner;
    }

    @Override
    public boolean end() {
        // check if it's the end of the game
        endGame = true;
        for (Player player : players) {
            if (player.isPlaying()) {
                endGame = false;
            }
        }

        return endGame;
    }

    @Override
    public String toString() {
        return "CardGame{" +
                "name='" + name + '\'' +
                '}';
    }
}
